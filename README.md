
<h1 align="center">
  <br>
  <a href="http://www.amitmerchant.com/electron-markdownify"><img src="https://gitlab.com/Diviercadavid/cryptoswiftuiapp/-/raw/main/imgs/logo.png?ref_type=heads" alt="Markdownify" width="200"></a>
  <br>
  CryptoSwiftuiAPP
  <br>
</h1>

<h4 align="center">A minimal App about crypto rates and details.</h4>

<p align="center">
  <a href="https://badge.fury.io/js/electron-markdownify">
    <img src="https://img.shields.io/badge/Swift-FA7343?style=for-the-badge&logo=swift&logoColor=white"
         alt="Gitter">
  </a>
</p>

<p align="center">
  <a href="#key-features">Persistence</a> •
  <a href="#how-to-use">SwiftUI</a> •
  <a href="#download">URLSession</a> •
  <a href="#credits">MVVM</a> •
  <a href="#related">Chart</a> •
  <a href="#license">License</a>
</p>

![screenshot](https://gitlab.com/Diviercadavid/cryptoswiftuiapp/-/raw/main/imgs/portfolio%20workflow.gif?ref_type=heads)
![screenshot](https://gitlab.com/Diviercadavid/cryptoswiftuiapp/-/raw/main/imgs/chart%20detail.gif?ref_type=heads)

## About CryptoSwiftUIAPP
 This app was made by following a @SwiftfulThinking course on youtube. It uses MVVM Architecture, Combine, and CoreData
* It shows all current and availables Coins in the market 
  - Bitcoin, Ethereum, Dash, Ripple, etc
* Allow create own portfolio and save in local
  - Saving data in CoraData
* Show a historical chart
* Show details about the coin selected
* Comunication with coingecko using URLSession
* Dark/Light mode
* Made with MVVM arquitecture
* Useing SwiftUI and StackNavigation

![screenshot](https://gitlab.com/Diviercadavid/cryptoswiftuiapp/-/raw/main/imgs/coingecko.png?ref_type=heads)

## Credits

This software uses the following open source packages:

- [Coingecko API](https://www.coingecko.com)
- [Youtube Channel](https://www.youtube.com/c/swiftfulthinking)
- [Buy a coffee to creator (nicksarno) 🤙🏻](https://www.buymeacoffee.com/nicksarno)
- [Developed by Divier Cadavid ](https://www.linkedin.com/in/diviercadavid/)


## Thanks for watching


## 
