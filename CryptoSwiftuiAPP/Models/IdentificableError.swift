//
//  IdentificableError.swift
//  CryptoSwiftuiAPP
//
//  Created by Divier on 17/10/2023.
//

import Foundation

struct IdentificableError: Identifiable {
    let id = UUID()
    let error: Error?
}
