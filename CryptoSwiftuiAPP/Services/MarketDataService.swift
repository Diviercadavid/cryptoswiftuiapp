//
//  MarketDataService.swift
//  CryptoSwiftuiAPP
//
//  Created by Divier on 3/10/2023.
//

import Foundation
import Combine

class MarketDataService {
    
    @Published var marketData: MarketDataModel? = nil
    @Published var error: Error? = nil
    var marketDataSubscription: AnyCancellable?
    
    init() {
        getData()
    }
    
    func getData() {
        
        guard let url = URL(string: "https://api.coingecko.com/api/v3/global")
        else { return }
        
        marketDataSubscription =  NetworkingManager.download(url: url)
            .decode(type: GlobalData.self, decoder: JSONDecoder())
            .receive(on: DispatchQueue.main)
            .sink(receiveCompletion: handlerError(completion:),
                  receiveValue: { [weak self] marketDataResponse in
                self?.marketData = marketDataResponse.data
                self?.marketDataSubscription?.cancel()
            })
        
    }
    

}

extension MarketDataService {
    private func handlerError(completion: Subscribers.Completion<Error>) {
        if case let .failure(error) = completion {
            NetworkingManager.handleCompletionError(error) { [weak self] handledError in
                guard let self = self else { return }
                self.error = handledError as Error
            }
        }
    }
}

