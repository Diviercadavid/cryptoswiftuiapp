//
//  PortfolioDataServices.swift
//  CryptoSwiftuiAPP
//
//  Created by Divier on 4/10/2023.
//

import Foundation
import CoreData

class PortfolioDataServices {
    
    private let container: NSPersistentContainer
    private let containerName = "PortfolioContainer"
    private let entityName = "PortfolioEntity"
    
    @Published var savedEntities: [PortfolioEntity] = []
    
    init() {
        container = NSPersistentContainer(name: containerName)
        container.loadPersistentStores { (_, error) in
            if let error = error {
                print("Error leading Core Data\(error)")
            }
        }
        getPortfolio()
    }
    
    // MARK: Public
    func updatePortfolio(coin: CoinModel, amount: Double) {
        
        if let entity = savedEntities.first(where: {$0.coinID == coin.id}) {
            if amount > 0 {
                update(entity: entity, amount: amount)
            } else {
                remove(entity: entity)
            }
        } else {
            add(coin: coin, amount: amount)
        }
    }
    
    // MARK: Private
    private func getPortfolio() {
        let request = NSFetchRequest<PortfolioEntity>(entityName: entityName)
        
        do {
            try savedEntities = container.viewContext.fetch(request)
        } catch {
            print("Error getting fetching portfolio entities. \(error)")
        }
    }
    
    private func add(coin: CoinModel, amount: Double) {
        let entity = PortfolioEntity(context: container.viewContext)
        entity.coinID = coin.id
        entity.amount = amount
        applyChanges()
    }
    
    private func update(entity: PortfolioEntity, amount: Double) {
        entity.amount = amount
        applyChanges()
    }
    
    private func remove(entity: PortfolioEntity) {
        container.viewContext.delete(entity)
        applyChanges()
    }
    
    private func save() {
        
        do {
            try container.viewContext.save()
        } catch {
            print("error saving to Core Dat. \(error)")
        }
        
    }
    
    private func applyChanges() {
        save()
        getPortfolio()
    }
}
