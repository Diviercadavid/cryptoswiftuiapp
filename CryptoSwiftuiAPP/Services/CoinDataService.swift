//
//  CoinDataService.swift
//  CryptoSwiftuiAPP
//
//  Created by Divier on 28/9/2023.
//

import Foundation
import Combine

class CoinDataService {
    
    @Published var allCoins: [CoinModel] = []
    @Published var coinDetail: CoinDetailModel?
    @Published var error: Error? = nil
    var coinsSubscription: AnyCancellable?
    
    init() { }
    
    func getCoins() {
        
        guard let url = URL(string: "https://api.coingecko.com/api/v3/coins/markets?vs_currency=usd&order=market_cap_desc&per_page=100&page=1&sparkline=true&price_change_percentage=24h&locale=en")
        else { return }
        
        coinsSubscription =  NetworkingManager.download(url: url)
            .decode(type: [CoinModel].self, decoder: JSONDecoder())
            .receive(on: DispatchQueue.main)
            .sink(receiveCompletion: handlerError(completion:)) { [weak self] returnedCoins in
                self?.allCoins = returnedCoins
                self?.coinsSubscription?.cancel()
            }
            
    }
    
    func getCoinDetail(coinID: String) {
        
        guard let url = URL(string: "https://api.coingecko.com/api/v3/coins/\(coinID)?localization=false&tickers=false&market_data=false&community_data=false&developer_data=false&sparkline=false")
                
        else { return }
        
        coinsSubscription =  NetworkingManager.download(url: url)
            .decode(type: CoinDetailModel.self, decoder: JSONDecoder())
            .receive(on: DispatchQueue.main)
            .sink(receiveCompletion: handlerError(completion:)) { [weak self] returnedCoin in
                self?.coinDetail = returnedCoin
                self?.coinsSubscription?.cancel()
            }
            
    }
    
}

extension CoinDataService {
    private func handlerError(completion: Subscribers.Completion<Error>) {
        if case let .failure(error) = completion {
            NetworkingManager.handleCompletionError(error) { [weak self] handledError in
                guard let self = self else { return }
                self.error = handledError as Error
            }
        }
    }
}
