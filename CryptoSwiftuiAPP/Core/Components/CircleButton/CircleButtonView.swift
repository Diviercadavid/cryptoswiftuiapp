//
//  CircleButtonView.swift
//  CryptoSwiftuiAPP
//
//  Created by Divier on 27/9/2023.
//

import SwiftUI

struct CircleButtonView: View {
    
    let iconeName: String
    
    var body: some View {
        Image(systemName: iconeName)
            .font(.headline)
            .foregroundColor(Color.theme.accent)
            .frame(width: 50, height: 50)
            .background {
                Circle().foregroundColor(Color.theme.background)
            }
            .shadow(color: Color.theme.accent.opacity(0.25), radius: /*@START_MENU_TOKEN@*/10/*@END_MENU_TOKEN@*/, x: /*@START_MENU_TOKEN@*/0.0/*@END_MENU_TOKEN@*/, y: /*@START_MENU_TOKEN@*/0.0/*@END_MENU_TOKEN@*/)
            .padding()
    }
}

#Preview {
    Group {
        CircleButtonView(iconeName: "info")
            .previewLayout(.sizeThatFits)
        
        CircleButtonView(iconeName: "plus")
            .previewLayout(.sizeThatFits)
            .colorScheme(.dark)
    }
    
    
}
