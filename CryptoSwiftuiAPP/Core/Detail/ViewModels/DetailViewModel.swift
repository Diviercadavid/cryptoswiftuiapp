//
//  DetailViewModel.swift
//  CryptoSwiftuiAPP
//
//  Created by Divier on 11/10/2023.
//

import Foundation
import Combine

class DetailViewModel: ObservableObject {
    
    @Published var coinDetail: CoinDetailModel?
    
    @Published var overviewStatistics: [StatisticModel] = []
    @Published var additionalStatistics: [StatisticModel] = []
    @Published var coin: CoinModel
    
    @Published var coinDescription: String? = nil
    @Published var websiteURL: String? = nil
    @Published var redditURL: String? = nil
    @Published var errorObject: IdentificableError? = nil
    
    
    private let coinDetailService: CoinDataService = CoinDataService()
    private var cancellable = Set<AnyCancellable>()
    
    init(coin: CoinModel) {
        self.coin = coin
        
        coinDetailService.getCoinDetail(coinID: coin.id)
        addSubscribers()
    }
    
    private func addSubscribers() {
        coinDetailService.$coinDetail
            .combineLatest($coin)
            .map(mapStatsModels)
            .dropFirst()
            .sink { [weak self] returnedArrays in
                self?.overviewStatistics = returnedArrays.overView
                self?.additionalStatistics = returnedArrays.additional
        }.store(in: &cancellable)
        
        coinDetailService.$coinDetail
            .sink(receiveValue: { [weak self] returnedCoinDetails in
                guard let self = self else { return }
                self.coinDescription = returnedCoinDetails?.readableDescription
                self.websiteURL = returnedCoinDetails?.links?.homepage?.first
                self.redditURL = returnedCoinDetails?.links?.subredditURL
            }).store(in: &cancellable)
        
        coinDetailService.$error
            .dropFirst()
            .map({IdentificableError(error: $0)})
            .sink { [weak self] returnedError in
                self?.errorObject = returnedError
            }
            .store(in: &cancellable)
    }
    
    private func mapStatsModels(coinDetailModel: CoinDetailModel?, coinModel: CoinModel) -> (overView:[StatisticModel], additional:[StatisticModel]) {
        
        // OverViews
        let overView: [StatisticModel] = createOverviewArray(coinDetailModel: coinDetailModel, coinModel: coinModel)
        
        // Additional
        let additionalArray: [StatisticModel] = createAdditionalArray(coinDetailModel: coinDetailModel, coinModel: coinModel)
        
        return (overView: overView, additional: additionalArray)
    }
    
    private func createOverviewArray(coinDetailModel: CoinDetailModel?, coinModel: CoinModel) -> [StatisticModel] {
        let price = coinModel.currentPrice.asCurrencyWithDecimals6()
        let pricePercentPercentChange = coinModel.priceChangePercentage24H
        let priceStat = StatisticModel(title: "Current Price", value: price, percentageChange: pricePercentPercentChange)
        
        let marketCap = "$ \(coinModel.marketCap?.formattedWithAbbreviations() ?? "")"
        let marketChange = coinModel.marketCapChangePercentage24H
        let marketCapsStat = StatisticModel(title: "Market Capitalisation", value: marketCap, percentageChange: marketChange)
        
        let rank = "\(coinModel.rank)"
        let rankStat = StatisticModel(title: "Rank", value: rank)
        
        let volume = "$\(coinModel.totalVolume?.formattedWithAbbreviations() ?? "")"
        let volumeStat = StatisticModel(title: "Volume", value: volume)
        
        return [priceStat, marketCapsStat, rankStat, volumeStat]
        
    }
    
    private func createAdditionalArray(coinDetailModel: CoinDetailModel?, coinModel: CoinModel) -> [StatisticModel] {
        let high = coinModel.high24H?.asCurrencyWithDecimals6() ?? "n/a"
        let highStat = StatisticModel(title: "24h High", value: high)
        
        let low = coinModel.low24H?.asCurrencyWithDecimals6() ?? "n/a"
        let lowStat = StatisticModel(title: "24h Low", value: low)
        
        let priceChange = coinModel.priceChange24H?.asCurrencyWithDecimals6() ?? "n/a"
        let pricePercentChange2 = coinModel.priceChangePercentage24H
        let priceChangeStat = StatisticModel(title: "24h Price Change", value: priceChange, percentageChange: pricePercentChange2)
        
        let marketCapChange = "$\(coinModel.marketCapChange24H?.formattedWithAbbreviations() ?? "")"
        let marketCapPercentChange2 = coinModel.marketCapChange24H
        let marketCapChangeStat = StatisticModel(title: "24h Market Cap Change", value: marketCapChange, percentageChange: marketCapPercentChange2)
        
        let blockTime = coinDetailModel?.blockTimeInMinutes ?? 0
        let blockTimeString = blockTime == 0 ? "n/a" : "\(blockTime)"
        let blockStat = StatisticModel(title: "Block Time", value: blockTimeString)
        
        let hashing = coinDetailModel?.hashingAlgorithm ?? "n/a"
        let hashingStat = StatisticModel(title: "Hashing Algorithm", value: hashing)
        
        return [highStat, lowStat, priceChangeStat, marketCapChangeStat, blockStat, hashingStat]
    }
}
