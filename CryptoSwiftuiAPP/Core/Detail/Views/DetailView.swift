//
//  DetailView.swift
//  CryptoSwiftuiAPP
//
//  Created by Divier on 9/10/2023.
//

import SwiftUI


// view avoid load Detail view if coin object is nil
struct DetailLoadingView: View {
    
    @Binding var coin: CoinModel?
    
    var body: some View {
        ZStack {
            if let coin = coin {
                DetailView(coin: coin)
            }
        }
    }
}

struct DetailView: View {
    
    
    @StateObject var detailViewModel: DetailViewModel
    @State private var showFullDescription: Bool = false
    
    private let columns: [GridItem] = [
        GridItem(.flexible()),
        GridItem(.flexible())
    ]
    private let spacing: CGFloat = 30
    
    init(coin: CoinModel) {
        _detailViewModel = StateObject(wrappedValue: DetailViewModel(coin: coin))
    }
    
    var body: some View {
        
        ScrollView {
            VStack(spacing: 20) {
                
                ChartView(coin: detailViewModel.coin)
                    .padding(.vertical)
                
                VStack {
                    overviewTitleSection
                    
                    Divider()
                    
                    descriptionView
                    
                    overViewGrid
                    
                    additionalTitleSection
                    
                    Divider()
                    
                    additionalViewGrid
                    
                    websitesSection
                    
                }
                .padding()
                
            }
            .background(Color.theme.background.ignoresSafeArea())
            .navigationTitle(detailViewModel.coinDetail?.name ?? "")
            .toolbar(content: {
                ToolbarItem(placement: .topBarTrailing) {
                    navigationBarTrailingItems
                }
            })
            .alert(item: $detailViewModel.errorObject) { identifiableError in
                Alert(title: Text("Error"), message: Text(identifiableError.error?.localizedDescription ?? ""), dismissButton: .default(Text("OK")))
            }
        }
        
    }
}

extension DetailView {
    
    private var navigationBarTrailingItems: some View {
        HStack {
            Text(detailViewModel.coin.symbol.uppercased())
                .font(.headline)
                .foregroundStyle(Color.theme.secondaryText)
            CoinImageView(coin: detailViewModel.coin)
                .frame(width: 25, height: 25)
        }
    }
    
    private var overviewTitleSection: some View {
        Text("Overview")
            .font(.title)
            .bold()
            .foregroundStyle(Color.theme.accent)
            .frame(maxWidth: .infinity, alignment: .leading)
    }
    
    private var additionalTitleSection: some View {
        Text("Additional Details")
            .font(.title)
            .bold()
            .foregroundStyle(Color.theme.accent)
            .frame(maxWidth: .infinity, alignment: .leading)
    }
    
    private var overViewGrid: some View {
        LazyVGrid(columns: columns,
                  alignment: .leading,
                  spacing: spacing,
                  pinnedViews: [],
                  content: {
            ForEach(detailViewModel.overviewStatistics) { stat in
                StatisticView(stat: stat)
            }
            
        })
    }
    
    private var additionalViewGrid: some View {
        LazyVGrid(columns: columns,
                  alignment: .leading,
                  spacing: spacing,
                  pinnedViews: [],
                  content: {
            ForEach(detailViewModel.additionalStatistics) { item in
                StatisticView(stat: item)
            }
        })
    }
    
    private var descriptionView: some View {
        ZStack {
            if let coinDescription = detailViewModel.coinDescription,
               !coinDescription.isEmpty {
                VStack(alignment: .leading) {
                    Text(coinDescription)
                        .lineLimit(showFullDescription ? nil : 3)
                        .font(.callout)
                        .foregroundStyle(Color.theme.secondaryText)
                    Button {
                        withAnimation(.easeOut) {
                            showFullDescription.toggle()
                        }
                    } label: {
                        Text( showFullDescription ? "Less..." : "Read more...")
                            .foregroundStyle(Color.blue)
                            .fontWeight(.bold)
                            .padding(.vertical, 8)
                    }
                    
                    
                }
                
            }
        }
    }
    
    private var websitesSection: some View {
        
        VStack(alignment: .leading, spacing: 10, content: {
            if let webSiteString = detailViewModel.websiteURL,
               let url = URL(string: webSiteString) {
                Link("Website", destination: url)
            }
            
            if let redditString = detailViewModel.redditURL,
               let url = URL(string: redditString) {
                Link("Reddit", destination: url)
            }
        })
        .foregroundStyle(Color.blue)
        .frame(maxWidth: .infinity, alignment: .leading)
        .font(.headline)
    }
}

#Preview {
    NavigationView {
        DetailView(coin: DeveloperPreview.instance.coin)
        
    }
    
}
