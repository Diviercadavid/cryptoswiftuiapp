//
//  LaunchView.swift
//  CryptoSwiftuiAPP
//
//  Created by Divier on 17/10/2023.
//

import SwiftUI

struct LaunchView: View {
    
    @State private var loadingText: String = "Loading your portfolio..."
    @State private var showLoadingText: Bool = false
    @Binding var showLaunchView: Bool
    
    var body: some View {
        ZStack {
            Color.launch.background
                .ignoresSafeArea()
            
            Image("logo-transparent")
                .resizable()
                .frame(width: 100, height: 100)
            
            Text(loadingText)
                .font(.headline)
                .fontWeight(.heavy)
                .foregroundStyle(Color.launch.accent)
                .transition(AnyTransition.scale.animation(.easeIn))
                .offset(y: 70)
        }
        .onAppear {
            showLoadingText.toggle()
            DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                showLaunchView.toggle()
            }
        }
    }
}

#Preview {
    LaunchView(showLaunchView: .constant(true))
}
