//
//  HomeViewModel.swift
//  CryptoSwiftuiAPP
//
//  Created by Divier on 28/9/2023.
//

import Foundation
import Combine

class HomeViewModel: ObservableObject {
    
    @Published var statistics: [StatisticModel] = []
    
    @Published var allCoins: [CoinModel] = []
    @Published var portfolioCoins: [CoinModel] = []
    @Published var searchText: String = ""
    @Published var isRefreshing: Bool = true
    @Published var sortOption: SortOption = .holdings
    
    @Published var errorObject: IdentificableError? = nil
    
    private var coinService = CoinDataService()
    private var marketService = MarketDataService()
    private var portfolioDataServices = PortfolioDataServices()
    private var cancellables = Set<AnyCancellable>()
    
    enum SortOption {
        case rank, rankReversed, holdings, holdingsReversed, price, priceReversed
    }
    
    init() {
        coinService.getCoins()
        subscribeObservers()
    }
    
    func subscribeObservers() {
        
        //Update Coins
        $searchText
            .delay(for: .seconds(0.5), scheduler: DispatchQueue.main)
            .combineLatest(coinService.$allCoins, $sortOption)
            .map(filterAndSortCoins)
            .sink { [weak self] values in
                self?.allCoins = values
            }
            .store(in: &cancellables)
        
        
        //update portfolios
        $allCoins
            .combineLatest(portfolioDataServices.$savedEntities)
            .map(mapAllCoinsToPortfolioCoins)
            .sink { [weak self] returnedCoins in
                guard let self = self else { return }
                self.portfolioCoins = self.sortPortfolioCoinsIfNeeded(coins: returnedCoins)
            }
            .store(in: &cancellables)
        
        
        //Update marketData
        marketService.$marketData
            .combineLatest($portfolioCoins)
            .map(mapGlobalMarketData)
            .sink { [weak self] returnedStats in
                self?.statistics = returnedStats
                self?.updateLoading(isRefreshing: false)
            }.store(in: &cancellables)
        
        //Getting errors
        marketService.$error
            .combineLatest(coinService.$error)
            .map(mapErrorsFromServices)
            .dropFirst()
            .sink { [weak self] handledError in
                self?.errorObject = handledError
            }
            .store(in: &cancellables)
    }
    
    func updateLoading(isRefreshing: Bool) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            self.isRefreshing = isRefreshing
        }
    }
    
    func updatePortfolio(coin: CoinModel, amount: Double) {
        portfolioDataServices.updatePortfolio(coin: coin, amount: amount)
    }
    
    func reloadData() {
        self.isRefreshing = true
        self.coinService.getCoins()
        self.marketService.getData()
        
    }
    
    private func filterAndSortCoins(text: String, coins:[CoinModel], shortOption: SortOption) -> [CoinModel] {
        var updatedCoins = filterCoins(text: text, coins: coins)
        sortCoins(sort: shortOption, coins: &updatedCoins)
        return updatedCoins
    }
    
    
    // inout: allow modify this instance and its is used to returned. returning the same instance of that memory
    private func sortCoins(sort: SortOption, coins: inout [CoinModel]){
        switch sort {
        case .rank, .holdings:
            coins.sort(by: { $0.rank < $1.rank })
        case .rankReversed, .holdingsReversed:
            coins.sort(by: { $0.rank > $1.rank })
        case .price:
            coins.sort(by: { $0.currentPrice < $1.currentPrice })
        case .priceReversed:
            coins.sort(by: { $0.currentPrice > $1.currentPrice })
        }
        
    }
    
    private func sortPortfolioCoinsIfNeeded(coins: [CoinModel]) -> [CoinModel] {
        switch sortOption {
        case .holdings:
            return coins.sorted(by: { $0.currentHoldingValue > $1.currentHoldingValue} )
        case .holdingsReversed :
            return coins.sorted(by: { $0.currentHoldingValue < $1.currentHoldingValue} )
        default :
            return coins
        }
        
    }
    
    private func filterCoins(text: String, coins:[CoinModel]) -> [CoinModel] {
        guard !text.isEmpty else { return coins }
        
        let searchingText = text.lowercased()
        
        return coins.filter({
            ($0.name.lowercased().contains(searchingText)) ||
            ($0.symbol.lowercased().contains(searchingText)) ||
            ($0.id.lowercased().contains(searchingText) )
        })
    }
    
    private func mapAllCoinsToPortfolioCoins(allCoins: [CoinModel], portfolioEntities: [PortfolioEntity]) -> [CoinModel] {
        allCoins.compactMap { coin -> CoinModel? in
            
            guard let entity = portfolioEntities.first(where: {$0.coinID == coin.id}) else { return nil }
            
            return coin.updateHolding(amount: entity.amount)
        }
    }
    
    private func mapGlobalMarketData(marketDataModel: MarketDataModel?, portfolioCoins: [CoinModel]) -> [StatisticModel] {
        var stats: [StatisticModel] = []
        
        guard let data = marketDataModel else { return stats }
        
        let marketCap = StatisticModel(title: "Market Cap",
                                       value: data.marketCap,
                                       percentageChange: data.marketCapChangePercentage24HUsd)
        
        let volume = StatisticModel(title: "24h Volume",
                                    value: data.volume)
        
        let btcDominance = StatisticModel(title: "BTC Dominance",
                                          value: data.btcDominance)
        
        let portfolioValue = portfolioCoins.map({$0.currentHoldingValue}).reduce(0, +)
        
        let previousValue = portfolioCoins.map { coin -> Double in
            let currentValue = coin.currentHoldingValue
            let percentChange = (coin.priceChangePercentage24H ?? 0) / 100
            let previousValue = currentValue / (1 + percentChange)
            return previousValue
        }.reduce(0, +)
        
        
        let percentageChange = ((portfolioValue - previousValue) / previousValue) * 100
        let portfolio = StatisticModel(title: "Portfolio Value", value: portfolioValue.asCurrencyWithDecimals2(), percentageChange: percentageChange)
        
        stats.append(contentsOf: [marketCap,
                                  volume,
                                  btcDominance,
                                  portfolio])
        
        
        return stats
    }
    
    private func mapErrorsFromServices(marketServiceError: Error?, coinServiceError: Error?) -> IdentificableError? {
        if let marketServiceError = marketServiceError {
            return IdentificableError(error: marketServiceError)
        }
        
        if let coinServiceError = coinServiceError {
            return IdentificableError(error: coinServiceError)
        }
         return nil
    }

}
